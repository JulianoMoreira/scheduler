﻿
namespace Scheduler.Shared
{
    public static class Runtime
    {
        private static string _connectionString;
        private static string _connectionStringDev = "server=localhost;database=equinix;uid=equinix;pwd=equinix;";
        public static string ConnectionString
        {
            get => string.IsNullOrEmpty(_connectionString) ? _connectionStringDev : _connectionString;
            set => _connectionString = value;
        }
    }
}
