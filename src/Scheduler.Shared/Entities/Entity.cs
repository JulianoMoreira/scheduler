﻿
using System;
using Flunt.Notifications;
using Flunt.Validations;

namespace Scheduler.Shared.Entities
{
    public abstract class Entity : Notifiable, IValidatable
    {
        protected Entity()
        {
            Id = Guid.NewGuid();
            CreateAt = DateTime.Now;
            UpdateAt = DateTime.Now;
        }

        public Guid Id { get; private set; }
        public DateTime CreateAt { get; private set; }
        public DateTime UpdateAt { get; private set; }

        public void Validate()
        {
            AddNotifications(new Contract()
                    .Requires()
                    .IsNotNullOrEmpty(Id.ToString(), "Id", "Id required.")
                    .IsNotNull(CreateAt, "CreateAt", "CreateAt required.")
                    .IsNotNull(UpdateAt, "UpdateAt", "UpdateAt required."));

            
        }
    }
}
