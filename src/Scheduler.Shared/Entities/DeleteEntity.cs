﻿using Flunt.Validations;

namespace Scheduler.Shared.Entities
{
    public class DeleteEntity : Entity
    {
        public DeleteEntity() 
        {
            Deleted = false;
        }

        public bool Deleted { get; private set; }
        public void Delete() => Deleted = true;

        protected new void Validate()
        {
            base.Validate();
            AddNotifications(new Contract()
                .Requires()
                .IsNotNull(Deleted, "Deleted", "Deleted required."));
        }
    }
}
