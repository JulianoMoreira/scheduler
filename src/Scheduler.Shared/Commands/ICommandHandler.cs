﻿
namespace Scheduler.Shared.Commands
{
    public interface ICommandHandler<in T> where T : ICommand
    {
        ICommand Handle(T command);
    }
}
