﻿using System;
using Flunt.Validations;
using Scheduler.Shared.Entities;

namespace Scheduler.Domain.Entities
{
    public class Schedule : Entity
    {
        protected Schedule() { }

        public Schedule(Server server, DateTime dayTime)
        {
            DayTime = dayTime;
            Server = server;
            Validate();
        }
        public DateTime DayTime { get; private set; }
        public Server Server { get; private set; }

        protected new void Validate()
        {
            base.Validate();
            AddNotifications(new Contract()
                .Requires()
                .IsNotNull(Server, "Server", "Server is required.")
                .IsNotNull(DayTime, "DayTime", "DayTime is required.")
                .IsGreaterThan(DayTime, DateTime.Now, "DayTime", "DayTime must be greater than today"));
            Server.Validate();
            AddNotifications(Server.Notifications);
        }
    }
}
