﻿using Flunt.Validations;
using Scheduler.Shared.Entities;

namespace Scheduler.Domain.Entities
{
    public class Server : DeleteEntity
    {
        protected Server() { }

        public Server(string name, string ip)
        {
            Name = name;
            Ip = ip;
            Validate();
        }

        public string Name { get; private set; }
        public string NameFull => $"{Name} - {Ip}";
        public string Ip { get; private set; }

        protected new void Validate()
        {
            base.Validate();
            AddNotifications(new Contract()
                .Requires()
                .IsNotNullOrEmpty(Name, "Name", "Name is required.")
                .IsNotNullOrEmpty(Ip, "Ip", "Ip is required.")
                .Matchs(Ip, "^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$", "Ip", "Invalid Ip."));
        }

        public void Update(string name, string ip)
        {
            Name = name;
            Ip = ip;
            Validate();
        }

        public override string ToString()
        {
            return $"{Name} - {Ip}";
        }
    }
}
