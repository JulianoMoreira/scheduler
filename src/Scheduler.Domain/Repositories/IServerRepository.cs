﻿
using System;
using System.Collections.Generic;
using Scheduler.Domain.Entities;

namespace Scheduler.Domain.Repositories
{
    public interface IServerRepository
    {
        IEnumerable<Server> GetAll();
        Server Get(Guid id);
        void Save(Server server);
        void Update(Server server);
    }
}
