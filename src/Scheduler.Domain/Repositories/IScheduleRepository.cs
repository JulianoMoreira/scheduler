﻿using System;
using System.Collections.Generic;
using Scheduler.Domain.Entities;

namespace Scheduler.Domain.Repositories
{
    public interface IScheduleRepository
    {
        IEnumerable<Schedule> GetAll();
        Schedule Get(Guid id);
        void Save(Schedule schedule);
        void Update(Schedule schedule);
        void Delete(Schedule schedule);
    }
}
