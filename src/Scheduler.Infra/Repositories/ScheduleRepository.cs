﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Scheduler.Domain.Entities;
using Scheduler.Domain.Repositories;
using Scheduler.Infra.Contexts;

namespace Scheduler.Infra.Repositories
{
    public class ScheduleRepository : IScheduleRepository
    {
        private readonly Context _context;

        public ScheduleRepository(Context context)
        {
            _context = context;
        }

        public Schedule Get(Guid id)
        {
            return _context.Schedules.Include(p => p.Server).FirstOrDefault(i => i.Id == id);
        }

        public IEnumerable<Schedule> GetAll()
        {
            return _context.Schedules.Include(p => p.Server).Where(i => i.DayTime >= DateTime.Now).ToList();
        }

        public void Save(Schedule schedule)
        {
            _context.Schedules.Add(schedule);
        }

        public void Update(Schedule schedule)
        {

            _context.Entry(schedule).State = EntityState.Modified;
        }
        public void Delete(Schedule schedule)
        {
            _context.Schedules.Remove(schedule);
        }
    }
}
