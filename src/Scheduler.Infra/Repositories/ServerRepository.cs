﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Scheduler.Domain.Entities;
using Scheduler.Domain.Repositories;
using Scheduler.Infra.Contexts;

namespace Scheduler.Infra.Repositories
{
    public class ServerRepository : IServerRepository
    {
        private readonly Context _context;

        public ServerRepository(Context context)
        {
            _context = context;
        }

        public IEnumerable<Server> GetAll()
        {
            return _context.Servers.Where(i => !i.Deleted).OrderBy(i => i.CreateAt).ToList();
        }

        public Server Get(Guid id)
        {
            return _context.Servers.FirstOrDefault(i => i.Id == id);
        }

        public void Save(Server server)
        {
            _context.Servers.Add(server);
        }

        public void Update(Server server)
        {
            _context.Entry(server).State = EntityState.Modified;
        }
    }
}
