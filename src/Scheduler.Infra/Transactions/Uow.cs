﻿using System.Threading.Tasks;
using Scheduler.Infra.Contexts;

namespace Scheduler.Infra.Transactions
{
    public class Uow : IUow
    {
        private readonly Context _context;

        public Uow(Context context)
        {
            _context = context;
        }

        public void Commit()
        {
            _context.SaveChanges();
        }

        public async Task CommitAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Rollback()
        {
            // EF resolve
        }
    }
}
