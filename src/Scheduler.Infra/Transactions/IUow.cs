﻿using System.Threading.Tasks;

namespace Scheduler.Infra.Transactions
{
    public interface IUow
    {
        void Commit();
        Task CommitAsync();
        void Rollback();
    }
}
