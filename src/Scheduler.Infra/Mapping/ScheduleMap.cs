﻿using Microsoft.EntityFrameworkCore;
using Scheduler.Domain.Entities;

namespace Scheduler.Infra.Mapping
{
    public static class ScheduleMap
    {
        public static ModelBuilder Map(ModelBuilder modelBuilder)
        {
            return modelBuilder.Entity<Schedule>(entity =>
            {
                entity.ToTable("schedules");
                entity.HasKey(p => p.Id);
                entity.Property(p => p.DayTime).IsRequired();
                entity.Property(p => p.CreateAt).IsRequired().ValueGeneratedOnAdd();
                entity.Property(p => p.UpdateAt).IsRequired().ValueGeneratedOnAddOrUpdate();
                entity.Ignore(p => p.Notifications);

                entity.HasOne(p => p.Server);
            });
        }
    }
}
