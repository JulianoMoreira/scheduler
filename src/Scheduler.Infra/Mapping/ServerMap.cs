﻿using Microsoft.EntityFrameworkCore;
using Scheduler.Domain.Entities;

namespace Scheduler.Infra.Mapping
{
    public static class ServerMap
    {
        public static ModelBuilder Map(ModelBuilder modelBuilder)
        {
            return modelBuilder.Entity<Server>(entity =>
            {
                entity.ToTable("servers");
                entity.HasKey(p => p.Id);
                entity.Property(p => p.Ip).IsRequired().HasMaxLength(15);
                entity.Property(p => p.Name).IsRequired().HasMaxLength(40);
                entity.Property(p => p.CreateAt).IsRequired().ValueGeneratedOnAdd();
                entity.Property(p => p.UpdateAt).IsRequired().ValueGeneratedOnAddOrUpdate();
                entity.Property(p => p.Deleted).IsRequired();
                entity.Ignore(p => p.Notifications);
            });
        }
    }
}
