﻿using Flunt.Notifications;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Conventions.Internal;
using Scheduler.Domain.Entities;
using Scheduler.Infra.Mapping;


namespace Scheduler.Infra.Contexts
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> option) : base(option) { }

        public DbSet<Server> Servers { get; set; }
        public DbSet<Schedule> Schedules { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Removendo Delete on cascade
            modelBuilder.Ignore<CascadeDeleteConvention>();
            //Ignorando classe nas entidades.
            modelBuilder.Ignore<Notification>();

            //Adicionando maps de classes.
            ServerMap.Map(modelBuilder);
            ScheduleMap.Map(modelBuilder);
        }
    }
}
