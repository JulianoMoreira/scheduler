﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Scheduler.Shared;

namespace Scheduler.Infra.Contexts
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<Context>
    {

        public Context CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<Context>();
            builder.UseMySql(Runtime.ConnectionString);
            return new Context(builder.Options);
        }
    }
}
