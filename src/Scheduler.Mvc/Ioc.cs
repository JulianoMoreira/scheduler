﻿using Microsoft.Extensions.DependencyInjection;
using Scheduler.Domain.Repositories;
using Scheduler.Infra.Contexts;
using Scheduler.Infra.Repositories;
using Scheduler.Infra.Transactions;

namespace Scheduler.Mvc
{
    public static class Ioc
    {
        public static void Add(IServiceCollection services)
        {
            //Injeção de dependencia
            services.AddScoped<Context, Context>();
            services.AddTransient<IUow, Uow>();
            //Repositorios
            services.AddTransient<IServerRepository, ServerRepository>();
            services.AddTransient<IScheduleRepository, ScheduleRepository>();

            //Handlers
            //services.AddTransient<ConstructionCommandHandler, ConstructionCommandHandler>();
            //Services
        }

    }
}
