﻿using System;
using Microsoft.AspNetCore.Mvc;
using Scheduler.Domain.Entities;
using Scheduler.Domain.Repositories;
using Scheduler.Infra.Transactions;
using Scheduler.Mvc.Models;

namespace Scheduler.Mvc.Controllers
{
    public class ServersController : Controller
    {
        private readonly IUow _uow;
        private readonly IServerRepository _serverRepository;

        public ServersController(IUow uow, IServerRepository serverRepository)
        {
            _uow = uow;
            _serverRepository = serverRepository;
        }

        // GET: Servers
        public ActionResult Index()
        {
            return View(_serverRepository.GetAll());
        }

        // GET: Servers/Details/5
        public ActionResult Details(Guid id)
        {
            return View(_serverRepository.Get(id));
        }

        // GET: Servers/Create
        public ActionResult Create()
        {
            return View(new ServerViewModel());
        }

        // POST: Servers/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ServerViewModel serverViewModel)
        {
            try
            {
                var server = new Server(serverViewModel.Name, serverViewModel.Ip);
                if (server.Valid)
                {
                    _serverRepository.Save(server);
                    _uow.Commit();
                    return RedirectToAction(nameof(Index));
                }

                serverViewModel.AddNotifications(server.Notifications);

                return View(serverViewModel);
            }
            catch
            {
                serverViewModel.FatalError = true;
                return View(serverViewModel);
            }
        }

        // GET: Servers/Edit/5
        public ActionResult Edit(Guid id)
        {
            var server = _serverRepository.Get(id);
            var model = new ServerViewModel()
            {
                Id = server.Id,
                Ip = server.Ip,
                Name = server.Name
            };
            return View(model);
        }

        // POST: Servers/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Guid id, ServerViewModel serverViewModel)
        {
            try
            {
                var server = _serverRepository.Get(id);
                server.Update(serverViewModel.Name, serverViewModel.Ip);
                if (server.Valid)
                {
                    _serverRepository.Update(server);
                    _uow.Commit();
                    return RedirectToAction(nameof(Index));
                }

                serverViewModel.AddNotifications(server.Notifications);

                return View(serverViewModel);
            }
            catch
            {
                serverViewModel.FatalError = true;
                return View(serverViewModel);
            }
        }


        // GET: Servers/Delete/5
        public ActionResult Delete(Guid id)
        {
            return View(_serverRepository.Get(id));
        }

        // POST: Servers/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Guid id, ServerViewModel serverViewModel)
        {
            try
            {
                var server = _serverRepository.Get(id);
                server.Delete();
                _serverRepository.Update(server);
                _uow.Commit();
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}