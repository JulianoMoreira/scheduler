﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Scheduler.Domain.Entities;
using Scheduler.Domain.Repositories;
using Scheduler.Infra.Transactions;
using Scheduler.Mvc.Models;

namespace Scheduler.Mvc.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUow _uow;
        private readonly IServerRepository _serverRepository;
        private readonly IScheduleRepository _scheduleRepository;

        public HomeController(IUow uow, IScheduleRepository scheduleRepository, IServerRepository serverRepository)
        {
            _uow = uow;
            _scheduleRepository = scheduleRepository;
            _serverRepository = serverRepository;
        }

        public IActionResult Index()
        {
            return View(_scheduleRepository.GetAll().OrderBy(i => i.DayTime));
        }

        public ActionResult Create()
        {
            var model = new ScheduleViewModel();
            foreach (var server in _serverRepository.GetAll())
                model.Servers.Add(new SelectListItem { Value = server.Id.ToString(), Text = server.ToString() });


            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ScheduleViewModel scheduleViewModel)
        {
            try
            {
                foreach (var serverId in scheduleViewModel.ServerIds)
                {
                    var server = _serverRepository.Get(serverId);
                    var date = new DateTime(scheduleViewModel.Day.Year, scheduleViewModel.Day.Month,
                        scheduleViewModel.Day.Day,
                        scheduleViewModel.Time.Hour, scheduleViewModel.Time.Minute, 00);
                    var schedule = new Schedule(server, date);
                    if (schedule.Valid)
                        _scheduleRepository.Save(schedule);
                    else
                        scheduleViewModel.AddNotifications(schedule.Notifications);
                }

                if (scheduleViewModel.Valid)
                {
                    _uow.Commit();
                    return RedirectToAction(nameof(Index));
                }

                foreach (var svr in _serverRepository.GetAll())
                    scheduleViewModel.Servers.Add(new SelectListItem { Value = svr.Id.ToString(), Text = svr.ToString() });
                return View(scheduleViewModel);
            }
            catch
            {
                scheduleViewModel.FatalError = true;
                foreach (var svr in _serverRepository.GetAll())
                    scheduleViewModel.Servers.Add(new SelectListItem { Value = svr.Id.ToString(), Text = svr.ToString() });
                return View(scheduleViewModel);
            }
        }

        public ActionResult Delete(Guid id)
        {
            return View(_scheduleRepository.Get(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Guid id, ServerViewModel serverViewModel)
        {
            try
            {
                var schedule = _scheduleRepository.Get(id);
                _scheduleRepository.Delete(schedule);
                _uow.Commit();
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
