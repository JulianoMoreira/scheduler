﻿
using System;
using System.Collections.Generic;
using Flunt.Notifications;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Scheduler.Mvc.Models
{
    public class ScheduleViewModel : Notifiable
    {
        public ScheduleViewModel()
        {
            FatalError = false;
            Servers = new List<SelectListItem>();
        }
        public Guid Id { get; set; }
        public Guid[] ServerIds { get; set; }
        public List<SelectListItem> Servers { get; set; }
        public DateTime Day { get; set; }
        public DateTime Time { get; set; }
        public bool FatalError { get; set; }
    }
}
