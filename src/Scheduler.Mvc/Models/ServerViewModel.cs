﻿using System;
using Flunt.Notifications;

namespace Scheduler.Mvc.Models
{
    public class ServerViewModel : Notifiable
    {
        public ServerViewModel()
        {
            FatalError = false;
        }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Ip { get; set; }
        public bool FatalError { get; set; }
    }
}
